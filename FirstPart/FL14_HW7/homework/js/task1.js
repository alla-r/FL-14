// Amount of defective batteries

const amountOfAllBatteries = prompt('Enter amount of batteries');
const percentageDefective = prompt('Enter % of defective batteries');

const errorMessage = () => {
  alert('Invalid input data');
}

if ( isNaN(amountOfAllBatteries) || isNaN(percentageDefective) ) {
  errorMessage();  
} else if ( Number(amountOfAllBatteries) < 0 || Number(percentageDefective) < 0 ) {
  errorMessage();
} else if ( Number(percentageDefective) > 100 ) {
  errorMessage();
} else {
  const amountDefective = (amountOfAllBatteries * percentageDefective / 100).toFixed(2);

  alert(`
    Amount of batteries: ${amountOfAllBatteries}
    Defective rate: ${Number(percentageDefective).toFixed(2)}%
    Amount of defective batteries: ${amountDefective}
    Amount of working batteries: ${(amountOfAllBatteries - amountDefective).toFixed(2)}
  `);
}