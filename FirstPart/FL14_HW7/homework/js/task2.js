// Middle character of the word

const input = prompt('Enter a word');

if (input.trim() === '' || input === '') {
  alert('Invalid value');
} else {
  const newInput = input.trim();
  const divider = 2;
  if ( newInput.length % divider === 1 ) {
    alert(`${newInput[(newInput.length - 1)/divider]}`);
  } else {
    const firstChar = newInput[newInput.length / divider - 1];
    const secondChar = newInput[newInput.length / divider];
    if (firstChar.toLowerCase() === secondChar.toLowerCase()) {
      alert('Middle characters are the same');
    } else {
      alert(`${firstChar}${secondChar}`);
    }
  }
}
