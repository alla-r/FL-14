// 1 - convert
function convert (...arg) {
  const arr = [];
  for (let i = 0; i < arg.length; i++) {
    if (typeof arg[i] === 'string') {
      arr.push(Number(arg[i]));
    } else {
      arr.push(String(arg[i]));
    }
  }
  return arr;
}
//console.log(convert('1', 2, 3, '4'));

// 2 - forEach
function executeforEach (arr, callback) {
  for ( let i=0; i < arr.length; i++ ) {
    callback(arr[i]);
  }
}
// console.log(executeforEach([1,2,3], function(el) {console.log(el*2)}));

// 3 - map
function mapArray (arr, callback) {
  let newArr = [];
  executeforEach(arr, function(el) { 
    if ( typeof el === 'string' && !isNaN(el) ) {
      newArr.push(callback(Number(el)));
    } else {
      newArr.push(callback(el));
    }
   });
  return newArr;
}
// console.log(mapArray([2, '5', 8], function(el) { return el + 3 }));

// 4 - filter
function filterArray (arr, callback) {
  let newArr = [];
  executeforEach(arr, function(el) {
    if ( callback(el) ) {
      newArr.push(el);
    }
  });
  return newArr;
}
// console.log(filterArray([2,5,8,10, 9], function(el) { return el % 2 === 0 }));

// 5 - get value position
function getValuePosition (arr, el) {
  let find = false;
  for ( let i = 0; i < arr.length; i++ ) {
    if ( arr[i] === el ) {
      find = i + 1;
    }
  }

  return find;
}
// console.log(getValuePosition([2,5,8], 8));
// console.log(getValuePosition([12,4,6], 1));

// 6 - reverse string
function flipOver (str) {
  let newStr = '';
  for ( let i = str.length-1; i >= 0; i-- ) {
    newStr += str[i];
  }
  
  return newStr;
}
// console.log(flipOver('hey world'));

// 7 - create array
function makeListFromRange (arr) {
  const newArr = [];
  if ( arr[0] <= arr[1] ) {
    for ( let i = arr[0]; i <= arr[1]; i++ ) {
      newArr.push(i);
    } 
  } else {
    for ( let i = arr[0]; i >= arr[1]; i-- ) {
      newArr.push(i);
    } 
  }
  
  return newArr;
}
// console.log(makeListFromRange([2, 7]));
// console.log(makeListFromRange([9, 2]));

// 8 - keys of object
function getArrayOfKeys ( arr, property ) {
  const newArr = [];
  executeforEach(arr, function(el) {
    if (el[property]) {
      newArr.push(el[property])
    }
  })

  return newArr;
}
// const fruits = [
//   { name: 'apple', weight: 0.5 },
//   { name: 'pineapple', weight: 2 }
// ];
// console.log(getArrayOfKeys(fruits, 'weight'));

// 9 - total weight
function getTotalWeight (arr) {
  let total = 0;
  executeforEach(arr, function(el) {
    total += el['weight'];
  });

  return total;
}
// const basket = [
//   { name: 'Bread', weight: 0.3 },
//   { name: 'Coca-cola', weight: 0.5 },
//   { name: 'Watermelon', weight: 8 }
// ];
// console.log(getTotalWeight(basket));

// 10 - day number that was some amount of days ago
function getPastDay ( date, number ) {
  const mlsec = number * 24 * 60 * 60 * 1000;
  const desiredDate = new Date(date - mlsec);

  return desiredDate.getDate();
}
// const date = new Date( 2020, 0, 2 );
// console.log(getPastDay( date, 1 ));
// console.log(getPastDay( date, 2 ));
// console.log(getPastDay( date, 365 ));

// 11 - format date
function makeTwoDigit (el) {
  if (el < 10) {
    el = '0' + el;
  }

  return el;
}

function formatDate( date ) {
  const year = date.getFullYear();
  let month = makeTwoDigit(date.getMonth() + 1);
  let day = makeTwoDigit(date.getDate());
  let hrs = makeTwoDigit(date.getHours()); 
  let min = makeTwoDigit(date.getMinutes());

  return `${year}/${month}/${day} ${hrs}:${min}`;
}
// console.log(formatDate( new Date('6/15/2019 09:15:00') ));
// console.log(formatDate(new Date()));