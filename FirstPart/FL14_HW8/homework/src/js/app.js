const startGameBtn = document.getElementById('btn-new-game');
const skipBtn = document.getElementById('btn-skip');
const questionSection = document.getElementById('question-section');
const winSection = document.getElementById('win-section');
const loseSection = document.getElementById('lose-section');

const newQuestions = JSON.parse(localStorage.getItem('questions'));
let listRandom = [];
let totalPrize = 0;
let currentPrize = 100;
const winnerNumber = 1000000;
const firstCurrentPrize = 100;

const startGame = (e) => {
  questionSection.style.display = 'block';
  skipBtn.className = 'btn';
  winSection.className = 'hide';
  loseSection.className = 'hide';
  listRandom = [];
  totalPrize = 0;
  currentPrize = firstCurrentPrize;

  e.preventDefault();
  
  const random = getRandomNumber(0, newQuestions.length);
  listRandom.push(random);
  ui(random);

  skipBtn.addEventListener('click', skipQuestion);
}

const skipQuestion = () => {
  const random = getRandomNumber(0, newQuestions.length);

  listRandom.push(random);
  ui(random);
  skipBtn.className = 'hide';
}

const ui = (id) => {
  questionSection.className = 'question-section';
  questionSection.innerHTML = `
    <div class="question-container">
    <div class="question" id="question">
      ${newQuestions[id]['question']}
    </div>
    <div id="answer-container" class="answer-container">
      <button class="answer-btn" value="0" id="answer1">
        ${newQuestions[id]['content'][0]}
      </button>
      <button class="answer-btn" value="1" id="answer2">
        ${newQuestions[id]['content'][1]}
      </button>
      <button class="answer-btn" value="2" id="answer3"> 
        ${newQuestions[id]['content'][2]}
      </button>
      <button class="answer-btn" value="3" id="answer4">
        ${newQuestions[id]['content'][3]}
      </button>
    </div>
  </div>

  <div class="output-container">
    <p id="total-prize">
      Total prize: ${totalPrize}
    </p>
    <p>
      Prize on current round: ${currentPrize}
    </p>
  </div>
  `
  document.getElementById('answer-container').addEventListener('click', (e) => answerClick(e, id));
}

const getRandomNumber = (min, max) => {
  const random = Math.floor(Math.random() * (max - min)) + min;
  const correctIndex = -1;
  if ( listRandom.indexOf(random) === correctIndex ) {
    return random;
  } else {
    return getRandomNumber(min, max);
  }
}

const answerClick = (e, id) => {
  if (Number(e.target.value) === newQuestions[id]['correct']) {
    correctAnswer();
  } else {
    gameOver();
  }
}

const correctAnswer = () => {
  totalPrize += currentPrize;
  currentPrize *= 2;

  if (totalPrize >= winnerNumber) {
    winGame();
  }

  const random = getRandomNumber(0, newQuestions.length);

  listRandom.push(random);
  ui(random);
}

const gameOver = () => {
  skipBtn.className = 'hide';
  questionSection.style.display = 'none';

  loseSection.className = 'end-section';
  loseSection.innerHTML = `Game over. Your prize is ${totalPrize}`;
}

const winGame = () => {
  skipBtn.className = 'hide';
  questionSection.style.display = 'none';

  winSection.className = 'end-section';
  winSection.innerHTML = `Congratulations! You won 1 000 000.`;
}

startGameBtn.addEventListener('click', startGame);