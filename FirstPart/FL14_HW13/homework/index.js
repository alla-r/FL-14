function getAge (date) {
  const diff = Date.now() - date.getTime();
  const ageDate = new Date(diff);
  const startYear = 1970;
  return ageDate.getUTCFullYear() - startYear; 
}

function getWeekDay (date) {
  const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  const day = date.getDay();
  return days[day];
}

function getProgrammersDay (year) {
  const september = 8;
  const dayLeapYear = 12;
  const dayCommonYear = 13;
  if (year % 4 === 0) {
    return `12 Sep, ${year} (${getWeekDay(new Date(year, september, dayLeapYear))})`
  } else {
    return `13 Sep, ${year} (${getWeekDay(new Date(year, september, dayCommonYear))})`
  }
}

function howFarIs (searchedDay) {
  if (searchedDay === getWeekDay(new Date())) {
    return `Hey, today is ${searchedDay} =)`;
  } else {
    let amount;
    const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    for (const day of days) {
      if (day === getWeekDay(new Date())) {
        if ( days.indexOf(day) > days.indexOf(searchedDay) ) {
          amount = 7 - days.indexOf(day) + days.indexOf(searchedDay);
        } else {
          amount = days.indexOf(searchedDay) - days.indexOf(day);
        }
      }
    }

    return `It's ${amount} day(s) left till ${searchedDay}`;
  }  
}

function isValidIdentifier (value) {
  const regexp = /^([a-z]|_|\$)+([a-z]|_|\$|\d)*$/gi;
  return regexp.test(value);
}

function capitalize (str) {
  return str.replace(/(^\w{1})|(\s{1}\w{1})/g, match => match.toUpperCase());
}

function isValidAudioFile (name) {
 const regexp = /^[a-z]+(\.mp3$)/gi;
 return regexp.test(name)
}

function getHexadecimalColors (str) {
  const regexp = /#([a-f0-9]){3}\b|#([a-f0-9]){6}\b/gi;
  return str.match(regexp) || [];
}

function isValidPassword (str) {
  const regexp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g;
  return regexp.test(str);
}

function addThousandsSeparators (value) {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}