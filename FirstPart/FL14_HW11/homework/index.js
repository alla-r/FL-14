const isEquals = (first, second) => first === second;

const numberToString = number => String(number);

const storeNames = (...arg) => arg;

const getDivision = (first, second) => first >= second ? first/second : second/first;

const negativeCount = arr => {
  let counter = 0;
  arr.forEach(element => {
    if (element < 0) {
      counter++;
    }
  });

  return counter;
}

const letterCount = (str, char) => {
  let counter = 0;
  for (let i = 0; i < str.length; i++) {
    if (str.charAt(i) === char) {
      counter++;
    }
  }
  return counter;
}

const countPoints = arr => {
  let points = 0;
  arr.forEach( el => {
    const result = el.split(':');

    if (Number(result[0]) > Number(result[1])) {
      points += 3;
    } else if (result[0] === result[1]) {
      points += 1;
    }
  });
  return points;
}
