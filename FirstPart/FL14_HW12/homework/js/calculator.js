try {
  const mathExp = prompt('Enter a mathematical expression');
  if (!mathExp || !mathExp.trim()) {
    throw new SyntaxError('You need to enter a mathematical expression. Please reload the page and try again');
  }
  let result = eval(mathExp);
  if (result === Infinity || result === -Infinity) {
    throw new Error('Incorrect a mathematical expression. Please reload the page and try again');
  }
  alert(`${mathExp} = ${result}`);
} catch (err) {
  alert(`${err}`);
}
