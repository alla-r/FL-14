function visitLink(path) {
	let counter = 0;
	if (localStorage.getItem(path)) {
		counter = Number(localStorage.getItem(path));
	}
	localStorage.setItem(path, counter + 1);
}

function viewResults() {
	const container = document.querySelector('.container');
	if (localStorage.length === 0) {
		const div = document.createElement('div');
		div.innerHTML = `You haven't visited pages`;
		container.appendChild(div);
	} else {
		const ul = document.createElement('ul');
		ul.className = 'visited-info';
		for (const key in localStorage) {
			if (localStorage.hasOwnProperty(key)) {
				const li = document.createElement('li');
				li.innerHTML = `
					You visited ${key} ${localStorage[key]} time(s)
				`;
				ul.appendChild(li);
			}
		}
		container.appendChild(ul);
		localStorage.clear();
	}
}
