'use strict';

let userLink = 'https://jsonplaceholder.typicode.com/users';

class HTTP {
  async get(url) {
    const response = await fetch(url);
    const resData = await response.json();
    return resData; 
  }

  async post(url, data) {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    const resData = await response.json();
    return resData; 
  }

  async put(url, data) {
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    const resData = await response.json();
    return resData; 
  }

  async delete(url) {
    const response = await fetch(url, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json'
      }
    });

    const resData = await response.json();
    return resData; 
  }
} 

const spinner = document.getElementById('spinner');

function showSpinner() {
  spinner.style.display = 'block';
}

function hideSpinner() {
  spinner.style.display = 'none';
}

const http = new HTTP();

const getUsers = () => {
  showSpinner();
  http.get(userLink)
    .then(data => renderListUI(data))
    .then(() => hideSpinner())
    .then(() => addBtnEventListeners())
    .catch(err => console.log(err));
}

getUsers();

function addBtnEventListeners () {
  document.getElementById('tbody').addEventListener('click', e => {
    if (e.target.className === 'delete-btn') {
      deleteUser(e.target.parentNode.parentNode.id)
    }
    if (e.target.className === 'edit-btn') {
      editUser(e.target, e.target.parentNode.parentNode.id)
    }
    if (e.target.className === 'user-name') {
      showPosts(e.target.parentNode.parentNode.id);
    }
  });
}

function showPosts(id) {
  console.log(id);
  document.getElementById('root').style.display = 'none';
  const container = document.getElementById('root2');
  

  let html = '';

  const linksForComments = [];
  showSpinner();
  http.get(`https://jsonplaceholder.typicode.com/posts?userId=${id}`)
    .then(posts => {
      console.log(posts);

      posts.forEach(post => {
        html += `
          <div id=post${post.id}>
            <h4>POST-${post.id} ${post.title}</h4>
            <div class="comments-container" style="margin-left: 1rem"></div>
          </div>         
        `;
      });

      container.innerHTML = `
        <h2>Posts by user with id ${id}</h2>
        ${html}
      `;
      
      posts.forEach(post => {
        linksForComments.push(http.get(`https://jsonplaceholder.typicode.com/posts/${post.id}/comments`));
      });
      showSpinner();
      Promise.all(linksForComments)
      .then(comments => {
        console.log(comments);

        comments.forEach((commentArr) => {
          let com = '';
          commentArr.forEach(el => {
            const commentsContainer = document.getElementById(`post${el.postId}`).querySelector('.comments-container');
            console.log(commentsContainer);
            com += `
              <div>comment name for post ${el.postId}: ${el.name}</div>
              <p style="padding-bottom: 1rem">comment body for post ${el.postId}: ${el.body}</p>
            `;

            commentsContainer.innerHTML = com;
          });
        });
      })
      .then(() => hideSpinner())
      .catch(err => console.log(err));      
    })
    .then(() => hideSpinner())
    .catch(err => console.log(err));
}

function renderListUI (data) {
  const tbody = document.getElementById('tbody');
  let rows = '';

  data.forEach( user => {
    rows += `
      <tr id=${user.id}>
        <td>
          <div class='user-id'>${user.id}</div>
        </td>
        <td>
          <div class='user-name' style='cursor: pointer'>${user.name}</div>
        </td>
        <td>
          <div class='user-email'>${user.email}</div>
        </td>
        <td>
          <button type='submit' class='edit-btn' style='cursor: pointer'>
            Edit
          </button>
        </td>
        <td>
          <button type='submit' class='delete-btn' style='cursor: pointer'>
            Delete
          </button>
        </td>
      </tr>
    `;
  });
 
  tbody.innerHTML = rows;
}

const deleteUser = (id) => {
  const confirmation = window.confirm(`Are you sure you want to delete user with id = ${id}?`);
  if (confirmation) {
    showSpinner();
    http.delete(userLink + `/${id}`)
      .then(() => alert('User is deleted'))
      .then(() => hideSpinner())
      .then(() => document.getElementById(id).remove())
      .catch(err => console.log(err));
  } 
}

function editUser (el, id) {
  const idInput = document.getElementById('userId');
  const nameInput = document.getElementById('userName');
  const emailInput = document.getElementById('userEmail');

  idInput.readOnly = true;
  idInput.value = id;
  nameInput.value = el.parentNode.parentNode.querySelector('.user-name').innerHTML;
  emailInput.value = el.parentNode.parentNode.querySelector('.user-email').innerHTML;
  
  document.getElementById('save-btn').addEventListener('click', (e) => {
    e.preventDefault();
    
    showSpinner();
    let idInput = document.getElementById('userId').value;
    let nameInput = document.getElementById('userName').value;
    let emailInput = document.getElementById('userEmail').value;
  
    const body = {
      name: nameInput,
      email: emailInput
    }

    http.put(userLink + `/${idInput}`, body)
      .then(data => updateHTML(data))
      .then(() => alert(`User was successfully update`))
      .then(() => hideSpinner())
      .then(() => {
        document.getElementById('userId').value = '';
        document.getElementById('userName').value = '';
        document.getElementById('userEmail').value = '';
      })
      .catch(err => console.log(err));
  });

  document.getElementById('cancel-btn').addEventListener('click', (e) => {
    e.preventDefault();
    idInput.value = '';
    nameInput.value = '';
    emailInput.value = '';
  });
}

function updateHTML(data) {
  document.getElementById(data.id).querySelector('.user-name').innerHTML = data.name;
  document.getElementById(data.id).querySelector('.user-email').innerHTML = data.email;
}