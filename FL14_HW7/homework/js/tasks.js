function maxElement(arr) {
  return Math.max(...arr);
}

function copyArray(arr) {
  return [...arr];
}

function addUniqueId (obj) {
  const newObj = JSON.parse(JSON.stringify(obj));
  newObj[Symbol.for('uniqueId')] = 1;
  return newObj;
}

function regroupObject(obj) {
  const {name: firstName, details: {id, age, university}} = obj;

  return {
    university,
    user: {
      age,
      firstName,
      id
    }
  }
}

function findUniqueElements(arr) {
  const newArr = [];
  for (let el of arr) {
    if ( !newArr.includes(Symbol.for(el)) ) {
      newArr.push(Symbol.for(el));
    }
  }
  return newArr
}

function hideNumber(number) {
  const last4Digits = number.slice(-4);
  return last4Digits.padStart(number.length, '*');
}

function isRequired () {
  throw new Error('Missing property');
}

function add(a = isRequired(), b = isRequired()) {
  return a + b;
}

function getUserName() {
  fetch('https://jsonplaceholder.typicode.com/users')
    .then(res => res.json())
    .then(data => {
      const arr = [];
      for (let user of data) {
        arr.push(user.name);
      }
      console.log(arr.sort());
    })
    .catch(err => console.log(err));
}

async function getUserName2() {
  const response = await fetch('https://jsonplaceholder.typicode.com/users');
  const resData = await response.json();

  const arr = [];
  for (let user of resData) {
    arr.push(user.name);
  }
  console.log(arr.sort());
}