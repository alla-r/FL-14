'use strict';

class Deck {
  constructor() {
    this.cards = [];

    const suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];
    const ranks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

    for (let i = 0; i < suits.length; i++) {
      for (let j = 0; j < ranks.length; j++) {
        this.cards.push(new Card(suits[i], ranks[j]));
      }      
    }
  }

  get count() {
    this._count = this.cards.length;
    return this._count;
  }

  shuffle() {
    for (let i = this.cards.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [this.cards[i], this.cards[j]] = [this.cards[j], this.cards[i]]
    }
  }

  draw(n) {
    return this.cards.splice(this.cards.length - n, this.cards.length);
  }
}

class Card {
  constructor(suit, rank) {
    this.suit = suit;
    this.rank = rank;
  }

  get isFaceCard() {
    this._isFaceCard = this.rank === 1 || this.rank > 10;
    return this._isFaceCard;
  }

  toString() {
    const ranks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    const rankName = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King'];

    return `${rankName[ranks.indexOf(this.rank)]} of ${this.suit}`;
  }

  compare(cardOne, cardTwo) {
    return cardOne.rank > cardTwo.rank;
  }
}

class Player {
  constructor(name, wins = 0) {
    this.name = name;
    this._wins = wins;
    this.deck = new Deck();
  }

  play(playerOne, playerTwo = this) {
    this.deck.shuffle();
    let scorePlayerOne = 0;
    let scorePlayerTwo = 0;

    function gameSession(cardPlayerOne, cardPlayerTwo) {
      if (cardPlayerOne.compare(cardPlayerOne, cardPlayerTwo)) {
        console.log(`${cardPlayerOne.toString()} bits ${cardPlayerTwo.toString()}`);
        scorePlayerOne++;
      } else if (cardPlayerTwo.compare(cardPlayerTwo, cardPlayerOne)) {
        console.log(`${cardPlayerTwo.toString()} bits ${cardPlayerOne.toString()}`);
        scorePlayerTwo++;
      } else {
        console.log(`${cardPlayerOne.toString()} equal ${cardPlayerTwo.toString()}`);
      }
    }

    const numberRounds = 15;

    for (let i = 0; i < numberRounds; i++) {
      const cardPlayerOne = this.deck.cards.shift();
      const cardPlayerTwo = this.deck.cards.shift();
      gameSession(cardPlayerOne, cardPlayerTwo);
    }

    while (scorePlayerOne === scorePlayerTwo) {
      const cardPlayerOne = this.deck.cards.shift();
      const cardPlayerTwo = this.deck.cards.shift();
      gameSession(cardPlayerOne, cardPlayerTwo);
    } 
    
    if (scorePlayerOne > scorePlayerTwo) {
      playerOne._wins++;
      return `${playerOne.name} wins ${scorePlayerOne} to ${scorePlayerTwo}`;
    } else if (scorePlayerOne < scorePlayerTwo) {
      playerTwo._wins++;
      return `${playerTwo.name} wins ${scorePlayerTwo} to ${scorePlayerOne}`;
    } 
  }
}


class Employee {
  static get EMPLOYEES() {
    if (this.list === undefined) {
      this.list = [];
    } 

    return this.list;
  }
  constructor (data) {
    this.id = data.id;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.birthday = data.birthday;
    this.salary = data.salary;
    this.position = data.position;
    this.department = data.department;
    Employee.EMPLOYEES.push(this);
  }

  get age() {
    const ageDifMs = Date.now() - this.birthday.getTime();
    const ageDate = new Date(ageDifMs);
    this._age = Math.abs(ageDate.getUTCFullYear() - 1970);
    return this._age;
  }

  get fullName() {
    this._fullName = `${this.firstName} ${this.lastName}`;
    return this._fullName;
  }

  quit() {
    Employee.EMPLOYEES.splice(Employee.EMPLOYEES.indexOf(this), 1);
  }

  retire() {
    console.log(`It was such a pleasure to work with you!`);
    Employee.EMPLOYEES.splice(Employee.EMPLOYEES.indexOf(this), 1);
  }

  getFired() {
    console.log(`Not a big deal!`);
    Employee.EMPLOYEES.splice(Employee.EMPLOYEES.indexOf(this), 1);
  }

  changeDepartment(newDepartment) {
    this.department = newDepartment;
  }

  changePosition(newPosition) {
    this.position = newPosition;
  }

  changeSalary(newSalary) {
    this.salary = newSalary;
  }

  getPromoted(benefits) {
    if (benefits.hasOwnProperty('salary')) {
      console.log('Yoohooo!');
      this.changeSalary(benefits.salary);
    }
    if (benefits.hasOwnProperty('position')) {
      console.log('Yoohooo!');
      this.changePosition(benefits.position);
    }
    if (benefits.hasOwnProperty('department')) {
      console.log('Yoohooo!');
      this.changeDepartment(benefits.department);
    }
  }

  getDemoted(punishment) {
    if (punishment.hasOwnProperty('salary')) {
      console.log('Damn!');
      this.changeSalary(punishment.salary);
    }
    if (punishment.hasOwnProperty('position')) {
      console.log('Damn!');
      this.changePosition(punishment.position);
    }
    if (punishment.hasOwnProperty('department')) {
      console.log('Damn!');
      this.changeDepartment(punishment.department);
    }
  }
}

class Manager extends Employee {
  constructor(data) {
    super(data);
    this.position = 'manager';
  }

  get managedEmployees () {
    this._managedEmployees = [];
    for (const employee of Employee.EMPLOYEES) {
      if (employee.department === this.department && employee.position !== 'manager') {
        this._managedEmployees.push(employee);
      }
    }
    return this._managedEmployees;
  }
}

class BlueCollarWorker extends Employee {
}

class HRManager extends Manager {
  constructor(data) {
    super(data);
    this.department = 'hr';
  }
}

class SalesManager extends Manager {
  constructor(data) {
    super(data);
    this.department = 'sales';
  }
}

function managerPro (manager) {
  manager.promoteEmployee = function(employee, benefits) {
    employee.getPromoted(benefits);
  }
  manager.demoteEmployee = function(employee, punishment) {
    employee.getDemoted(punishment);
  }
}