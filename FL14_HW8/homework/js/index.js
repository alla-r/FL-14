const $list = $(".list");
const $input = $("#add-input");
const $add = $("#add-submit");

const $filter = $('#filter-input');

let todos = [
  {
    text: "Buy milk",
    done: false
  },
  {
    text: "Play with dog",
    done: true
  }
];

loadEventListeners();

function loadEventListeners() {
  // load tasks from LS
  $(document).ready(function() {
    if (localStorage.getItem('todoList') !== null) {
      todos = JSON.parse(localStorage.getItem('todoList'));
    }
  
    todos.forEach( task => {
      let itemClassName = '';
      task.done === true ? itemClassName = 'item-text done' : itemClassName = 'item-text';
  
      $list.append(`
      <li class="item">
        <span class="${itemClassName}">${task.text}</span>
        <button class="item-remove">Remove</button>
      </li>
      `);
    });
  });

  $add.on('click', function(e) {
    e.preventDefault();
    if (!$input.val().trim()) {
      alert(`You should write a task description`);
    } else {
      storeTaskInLocalStorage({
        text: $input.val().trim(),
        done: false
      });

      $list.append(`
      <li class="item">
        <span class="item-text">${$input.val().trim()}</span>
        <button class="item-remove">Remove</button>
      </li>
      `);

      $input.val('');
    }  
  });

  $list.on('click', function(e) {
    if (e.target.classList.contains('item-remove')) {
      removeTaskFromLocalStorage(e.target.parentNode.querySelector('.item-text').innerHTML);

      $(e.target).parent().remove();
    }

    if (e.target.classList.contains('item-text')) {
      markTaskDoneInLocalStorage(e.target.parentNode.querySelector('.item-text').innerHTML);

      $(e.target).toggleClass('done');
    }
  });

  $filter.keyup(function(e) {
    const text = e.target.value.toLowerCase();
    $('.item-text').each(function() {
      const item = $(this).html();
      if (item.toLowerCase().indexOf(text) !== -1) {
        $(this).css('display', 'block');
      } else {
        $(this).parent().css('display', 'none');
      }
    });
  });
}

function storeTaskInLocalStorage(task) {
  if (localStorage.getItem('todoList') !== null) {
    todos = JSON.parse(localStorage.getItem('todoList'));
  }
  todos.push(task)
  localStorage.setItem('todoList', JSON.stringify(todos));
}

function removeTaskFromLocalStorage(taskItem) {
  if (localStorage.getItem('todoList') !== null) {
    todos = JSON.parse(localStorage.getItem('todoList'));
  }

  todos.forEach((todo, i) => {    
    if (todo.text === taskItem) {
      todos.splice(i, 1);
    }
  });

  localStorage.setItem('todoList', JSON.stringify(todos));
}

function markTaskDoneInLocalStorage(taskItem) {
  if (localStorage.getItem('todoList') !== null) {
    todos = JSON.parse(localStorage.getItem('todoList'));
  }

  todos.forEach((todo, i) => {    
    if (todo.text === taskItem) {
      todos[i].done === false ? todos[i].done = true : todos[i].done = false;
    }
  });

  localStorage.setItem('todoList', JSON.stringify(todos));
}